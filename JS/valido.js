
function formulario() {
    let { p_nombre, s_nombre, N_U, ciudad, elige, codigo, acepto } = Inputs();
    let { msj1, msj2, msj3, msj4, msj5, msj6, msj7 } = Mensajes();
    let { nombre, seg_nombre, user, ciudad1, localidad, codigopostal } = Data();
    if (p_nombre == "") {
        msj1.innerHTML = "Campo vacio, *Ingrese primer nombre*"
        return false;
    }else if (8 < p_nombre.length) {
        msj1.innerHTML = "Primer Nombre *excede caracteres* 8 caracteres Max."
        return false;
    }else if (s_nombre == "") {
        msj2.innerHTML = "Campo vacio, *Ingrese segundo nombre*"
        return false;
    }else if (8 < s_nombre.length) {
        msj2.innerHTML = "Segundo Nombre *excede caracteres* 8 caractesres Max."
        return false;
    }else if (N_U == "") {
        msj3.innerHTML = "Campo vacio, *Ingrese Nombre de Usuario*"
        return false;
    }else if (ciudad == "") {
        msj4.innerHTML = "Campo vacio, *Ingrese Ciudad*"
        return false;
    }else if (elige == "") {
        msj5.innerHTML = "Campo vacio, *Elija una Localidad"
        return false;
    }else if (codigo == "") {
        msj6.innerHTML = "Campo vacio, *Ingrese Codigo Postal*"
        return false;
    }else if (acepto == false) {
        msj7.innerHTML = "Campo vacio, *Aceptar Condiciones para continuar"
        return false;
    }else {
        DataFinal();
        InputsVacios();
        msj1 = document.getElementById("msj1") = "";
    }
        
    function DataFinal() {
        nombre.innerHTML = p_nombre;
        seg_nombre.innerHTML = s_nombre;
        user.innerHTML = N_U;
        ciudad1.innerHTML = ciudad;
        localidad.innerHTML = elige;
        codigopostal.innerHTML = codigo;
    }
    
    function Data() {
        let nombre = document.getElementById("nombre");
        let seg_nombre = document.getElementById("s_nombre");
        let user = document.getElementById("user");
        let ciudad1 = document.getElementById("ciudad");
        let localidad = document.getElementById("localidad");
        let codigopostal = document.getElementById("codigopostal");
        return { nombre, seg_nombre, user, ciudad1, localidad, codigopostal };
    }
    
}

function Inputs() {
    let p_nombre = document.getElementById("validationDefault01").value;
    let s_nombre = document.getElementById("validationDefault02").value;
    let N_U = document.getElementById("validationDefaultUsername").value;
    let ciudad = document.getElementById("validationDefault03").value;
    let elige = document.getElementById("validationDefault04").value;
    let codigo = document.getElementById("validationDefault05").value;
    let acepto = document.getElementById("invalidCheck2").checked;
    return { p_nombre, s_nombre, N_U, ciudad, elige, codigo, acepto };
}

function InputsVacios() {
    p_nombre = document.getElementById("validationDefault01").value = "";
    s_nombre = document.getElementById("validationDefault02").value = "";
    N_U = document.getElementById("validationDefaultUsername").value = "";
    ciudad = document.getElementById("validationDefault03").value = "";
    elige = document.getElementById("validationDefault04").value = "";
    codigo = document.getElementById("validationDefault05").value = "";
    acepto = document.getElementById("invalidCheck2").checked = "";
    
}

function Mensajes() {
    let msj1 = document.getElementById("msj1"), 
    msj2 = document.getElementById("msj2"), 
    msj3 = document.getElementById("msj3"), 
    msj4 = document.getElementById("msj4"), 
    msj5 = document.getElementById("msj5"), 
    msj6 = document.getElementById("msj6"), 
    msj7 = document.getElementById("msj7");
    return { msj1, msj2, msj3, msj4, msj5, msj6, msj7 };
}

function borrar() {
    msj1.innerHTML = ("");
    msj2.innerHTML = ("");
    msj3.innerHTML = ("");
    msj4.innerHTML = ("");
    msj5.innerHTML = ("");
    msj6.innerHTML = ("");
    msj7.innerHTML = ("");
}

function borrarTodo() {
    
}

function BM() {
    formulario()
    setTimeout(() => { 
    borrar()
    }, 5000)
}
